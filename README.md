## Start

**копіюємо .env**

`docker-compose down`

`docker-compose build`

`docker-compose up`

`docker-compose exec --user laravel php composer install`

`docker-compose exec --user root php php artisan dev:init`

## Google

Виконати перший крок https://developers.google.com/sheets/api/quickstart/php
Скопіювати CREDENTIALS, і json вставити в .env -> CREDENTIALS_GOOGLE_SHEETS

Потім виконати команду

`docker-compose exec php php artisan google:token`

## Queue

`docker-compose exec php  php artisan queue:work`


