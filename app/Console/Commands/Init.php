<?php


namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class Init extends Command
{
    protected $signature = 'dev:init';

    public function handle()
    {
        Artisan::call('key:generate');
        Artisan::call('config:clear');
        Artisan::call(' migrate:fresh --seed');

        exec("chown -R www-data:www-data " . base_path());
    }
}
