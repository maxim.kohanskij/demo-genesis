<?php


namespace App\Core\Events;

interface EventInterface
{
    public static function dispatch();
}
