<?php


namespace App\Core\Events;

interface OrderSavedEventInterface
{
    public function __construct(int $orderId);
}
