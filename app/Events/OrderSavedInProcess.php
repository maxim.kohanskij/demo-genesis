<?php

namespace App\Events;

use App\Core\Events\EventInterface;
use App\Core\Events\OrderSavedEventInterface;
use App\Models\Order;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderSavedInProcess implements EventInterface, OrderSavedEventInterface
{
    use Dispatchable, SerializesModels;

    protected Order $order;

    public function __construct(int $orderId)
    {
        $this->order = Order::findOrFail($orderId);
    }

    public function getOrder(): Order
    {
        return $this->order;
    }
}
