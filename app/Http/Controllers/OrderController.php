<?php

namespace App\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Resources\OrderResource;
use App\Http\Services\OrderService;

class OrderController extends Controller
{
    public function store(StoreOrderRequest $request, OrderService $orderService): OrderResource
    {
        $order = $orderService->processOrder($request);

        return new OrderResource($order);
    }
}
