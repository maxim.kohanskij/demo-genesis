<?php

namespace App\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Models\User;

class UserController extends Controller
{
    public function indexByRole(string $role): UserCollection
    {
        return new UserCollection(User::query()->where('role', '=', $role)->paginate());
    }

    public function show(int $id): UserResource
    {
        return new UserResource(User::findOrFail($id));
    }
}
