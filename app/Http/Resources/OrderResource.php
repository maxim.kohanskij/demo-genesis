<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getAttribute('id'),
            'product' => new ProductResource($this->getAttribute('productUser')->getAttribute('product')),
            'user_id' => $this->getAttribute('productUser')->getAttribute('user')->getAttribute('id'),
            'client' => new UserResource($this->getAttribute('client')),
            'price' => $this->getAttribute('price'),
            'currency' => $this->getAttribute('currency'),
            'status' => $this->getAttribute('status')
        ];
    }
}
