<?php

namespace App\Http\Resources;

use App\Models\ProductUser;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        $productUser = new ProductUser;
        return [
            'id' => $this->getAttribute('id'),
            'name' => $this->getAttribute('name'),
            'description' => $this->getAttribute('description'),
            'price' => $this->whenPivotLoaded($productUser, function () {
                    return $this->pivot->getAttribute('price');
            }),
            'currency' => $this->whenPivotLoaded($productUser, function () {
                return $this->pivot->getAttribute('currency');
            }),
            'product_user_id' => $this->whenPivotLoaded($productUser, function () {
                return $this->pivot->getAttribute('id');
            }),
        ];
    }
}
