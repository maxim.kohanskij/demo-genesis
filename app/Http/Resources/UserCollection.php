<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return $this->collection->map(function ($item) {
            return [
                'id' => $item->getAttribute('id'),
                'full_name' => $item->getAttribute('full_name'),
                'role' => $item->getAttribute('role'),
                'photo' => $item->getAttribute('photo'),
                'biography' => $item->getAttribute('biography')
            ];
        })->toArray();
    }
}
