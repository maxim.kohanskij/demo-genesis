<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->getAttribute('id'),
            'full_name' => $this->getAttribute('full_name'),
            'role' => $this->getAttribute('role'),
            'photo' => $this->getAttribute('photo'),
            'biography' => $this->getAttribute('biography'),
            'products' => new ProductCollection($this->getAttribute('products'))
        ];
    }
}
