<?php

namespace App\Http\Services;

use App\Http\Requests\StoreOrderRequest;
use App\Models\Order;
use App\Models\User;

class OrderService
{
    public function processOrder(StoreOrderRequest $request): Order
    {
        $request->validated();

        $client = User::query()
            ->where('email', '=', $request->email)
            ->firstOrCreate(['email' => $request->email, 'role' => User::ENUMS['roles']['client']]);

        $order = new Order();
        $order->setAttribute('product_user_id', $request->product_user_id);
        $order->setAttribute('client_id', $client->id);
        $order->setAttribute('status', Order::ENUMS['status']['InProcess']);
        $order->save();

        return $order;
    }
}
