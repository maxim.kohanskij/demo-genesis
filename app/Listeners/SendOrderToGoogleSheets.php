<?php

namespace App\Listeners;

use App\Events\OrderSavedInProcess;
use App\Models\Order;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderToGoogleSheets implements ShouldQueue
{
    protected const RANGE = 'A2';
    protected const VALUE_INPUT_OPTION = ['RAW'];

    public function handle(OrderSavedInProcess $event): void
    {
        $spreadsheetId = config('services.google.sheets.order_spreadsheet_id');

        $body = $this->createValueRange($event->getOrder());
        $params = ['valueInputOption' => self::VALUE_INPUT_OPTION];

        app(Google_Service_Sheets::class)->spreadsheets_values->append($spreadsheetId, self::RANGE, $body, $params);
    }

    protected function createValueRange(Order $order): Google_Service_Sheets_ValueRange
    {
        return new Google_Service_Sheets_ValueRange([
            'values' => [
                [
                    $order->getAttribute('client')->getAttribute('email'),
                    $order->getAttribute('productUser')->getAttribute('product')->getAttribute('name'),
                    $order->getAttribute('price'),
                    $order->getAttribute('currency'),
                ]
            ]
        ]);
    }
}
