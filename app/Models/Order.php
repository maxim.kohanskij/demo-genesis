<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Order extends Model
{
    protected $table = 'orders';

    public const ENUMS = [
        'status' => ['Pending' => 'Pending', 'InProcess' => 'InProcess', 'Error' => 'Error', 'Success' => 'Success']
    ];

    protected $attributes = [
        'status' => self::ENUMS['status']['Pending']
    ];

    protected $fillable = [
        'product_user_id',
        'client_id'
    ];

    /** The attributes that should be cast to native types. */
    protected $casts = [
        'price' => 'float',
        'currency' => 'string',
        'status' => 'string',
        'client_id' => 'int',
        'product_user_id' => 'int',
        'created_at' => 'date:d.m.Y H:m:s',
        'updated_at' => 'date:d.m.Y H:m:s',
    ];

    public function productUser(): HasOne
    {
        return $this->hasOne(ProductUser::class, 'id', 'product_user_id');
    }

    public function client(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'client_id');
    }
}
