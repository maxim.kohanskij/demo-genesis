<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    protected $table = 'products';

    protected $attributes = [
        'description' => ''
    ];

    protected $fillable = [
        'name',
        'description'
    ];

    /** The attributes that should be cast to native types. */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'created_at' => 'date:d.m.Y H:m:s',
        'updated_at' => 'date:d.m.Y H:m:s',
    ];

    public function users(): BelongsToMany
    {
        return $this
            ->belongsToMany(User::class)
            ->using(ProductUser::class)
            ->withPivot(['id', 'price', 'currency', 'created_at']);
    }
}
