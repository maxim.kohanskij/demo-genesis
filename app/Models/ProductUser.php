<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductUser extends Pivot
{
    protected $table = 'product_user';

    public $incrementing = true;

    protected $fillable = [
        'price',
        'currency'
    ];

    /** The attributes that should be cast to native types. */
    protected $casts = [
        'id' => 'int',
        'product_user_id' => 'int',
        'price' => 'float',
        'currency' => 'string',
        'created_at' => 'date:d.m.Y H:m:s',
        'updated_at' => 'date:d.m.Y H:m:s',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}
