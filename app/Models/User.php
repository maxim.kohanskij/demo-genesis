<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public const ENUMS = [
        'roles' =>['astrologer' => 'astrologer', 'client' => 'client']
    ];

    protected $table = 'users';

    protected $perPage = 5;

    protected $attributes = [
        'full_name' => '',
        'biography' => '',
        'photo' => ''
    ];

    protected $fillable = [
        'full_name',
        'photo',
        'email',
        'password',
        'biography',
        'role'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /** The attributes that should be cast to native types. */
    protected $casts = [
        'full_name' => 'string',
        'photo' => 'string',
        'email' => 'string',
        'biography' => 'string',
        'role' => 'string',
        'password' => 'string',
        'created_at' => 'date:d.m.Y H:m:s',
        'updated_at' => 'date:d.m.Y H:m:s',
    ];

    public function products(): BelongsToMany
    {
        return $this
            ->belongsToMany(Product::class)
            ->using(ProductUser::class)
            ->withPivot(['id', 'price', 'currency', 'created_at']);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }
}
