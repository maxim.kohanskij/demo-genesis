<?php

namespace App\Observers;

use App\Core\Events\EventInterface;
use App\Core\Events\OrderSavedEventInterface;
use App\Models\Order;
use Illuminate\Support\Str;

class OrderObserver
{
    public function creating(Order $order): void
    {
        $this->setPrice($order);
    }

    public function saved(Order $order): void
    {
        $this->dispatchEventStatus($order);
    }

    protected function dispatchEventStatus(Order $order): void
    {
        if ($order->wasChanged() === false || $order->wasChanged('status')) {
            $eventClass = 'App\Events\\OrderSaved' . Str::studly($order->getAttribute('status'));
            if (class_exists($eventClass)
                && is_subclass_of($eventClass, EventInterface::class)
                && is_subclass_of($eventClass, OrderSavedEventInterface::class)
            ) {
                $eventClass::dispatch($order->id);
            }
        }
    }

    protected function setPrice(Order $order): void
    {
        if ($order->getAttribute('price') === null && $order->getAttribute('product_user_id') !== null) {
            $order->setAttribute('price', $order->productUser->getAttribute('price'));
            $order->setAttribute('currency', $order->productUser->getAttribute('currency'));
        }
    }
}
