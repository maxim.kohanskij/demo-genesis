<?php

namespace App\Observers;

use App\Models\User;
use Illuminate\Support\Str;

class UserObserver
{
    public function creating(User $user): void
    {
        $this->setPassword($user);
    }

    protected function setPassword(User $user): void
    {
        if ($user->getAttribute('password') === null) {
            $user->setAttribute('password', Str::random(10));
        }
    }
}
