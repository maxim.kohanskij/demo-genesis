<?php

namespace App\Providers;

use Google_Client;
use Google_Service_Sheets;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->registerGoogleClient();
        $this->registerGoogleServiceSheets();
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }

    protected function registerGoogleServiceSheets(): void
    {
        $this->app->bind(Google_Service_Sheets::class, function () {
            $client = $this->app->make(Google_Client::class);
            return new Google_Service_Sheets($client);
        });
    }

    protected function registerGoogleClient(): void
    {
        $this->app->bind(Google_Client::class, function () {
            $client = new Google_Client();
            $client->setAuthConfig(config('services.google.sheets.credentials'));
            $client->setAccessType('offline');
            $client->setPrompt('select_account consent');
            $tokenPath = Storage::disk('local')->path('google-sheets-token.json');

            if (file_exists($tokenPath)) {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $client->setAccessToken($accessToken);
            }

            if ($client->isAccessTokenExpired() && $client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());

                if (!file_exists(dirname($tokenPath))) {
                    mkdir(dirname($tokenPath), 0700, true);
                }

                file_put_contents($tokenPath, json_encode($client->getAccessToken()));
            }

            return $client;
        });
    }
}
