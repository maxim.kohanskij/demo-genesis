<?php

return [
    'google' => [
        'sheets' => [
            'credentials' => json_decode(env('CREDENTIALS_GOOGLE_SHEETS'), true),
            'order_spreadsheet_id' => env('ORDERS_SPREADSHEET_ID'),
        ]
    ]
];
