<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createUsersTable();
        $this->createPasswordResetsTable();
        $this->createFailedJobsTable();
        $this->createJobsTable();

        $this->createProductsTable();
        $this->createProductUserTable();
        $this->createOrdersTable();
    }

    protected function createUsersTable()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->enum('role', ['astrologer', 'client']);//cтворюю прості ролі..а в реальному проекту, це був би звязок багато до багатьох
            $table->string('photo')->default('');
            $table->text('biography');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    protected function createPasswordResetsTable()
    {
        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
    }

    protected function createFailedJobsTable()
    {
        Schema::create('failed_jobs', function (Blueprint $table) {
            $table->id();
            $table->string('uuid')->unique();
            $table->text('connection');
            $table->text('queue');
            $table->longText('payload');
            $table->longText('exception');
            $table->timestamp('failed_at')->useCurrent();
        });
    }

    protected function createJobsTable()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('queue')->index();
            $table->longText('payload');
            $table->unsignedTinyInteger('attempts');
            $table->unsignedInteger('reserved_at')->nullable();
            $table->unsignedInteger('available_at');
            $table->unsignedInteger('created_at');
        });
    }

    protected function createProductsTable()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });
    }

    protected function createProductUserTable()
    {
        Schema::create('product_user', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('product_id');
            $table->float('price');
            $table->string('currency', 3);
            $table->timestamps();

            $table->unique(['user_id', 'product_id']);

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');

            $table
                ->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('restrict');
        });
    }

    protected function createOrdersTable()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('product_user_id');
            $table->unsignedBigInteger('client_id');
            $table->float('price');
            $table->string('currency', 3);
            $table->enum('status', ['Pending', 'InProcess', 'Error', 'Success']);
            $table->timestamps();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict');
            $table
                ->foreign('product_user_id')
                ->references('id')
                ->on('product_user')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('failed_jobs');
        Schema::dropIfExists('jobs');

        Schema::dropIfExists('orders');
        Schema::dropIfExists('product_user');

        Schema::dropIfExists('users');
        Schema::dropIfExists('products');

    }
}
