<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    public function __construct(protected Generator $faker)
    {
    }

    public function run()
    {
        $this->createProducts();
        $this->createUsers();

        $this->createProductAstrologer();
    }

    protected function createProducts()
    {
        $products = ['Натальная карта', 'Детальный гороскоп', 'Отчет совместимости', 'Гороскоп на год'];
        foreach ($products as $product) {
            DB::table('products')->insert([
                'name' => $product,
                'description' => $this->faker->realText(),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }

    protected function createUsers(int $count = 12)
    {
        for ($i = 0; $i < $count; $i++) {
            DB::table('users')->insert([
                'email' => $this->faker->unique()->safeEmail,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'remember_token' => Str::random(10),
                'role' => $this->faker->randomElement(User::ENUMS['roles']),
                'full_name' => $this->faker->name,
                'biography' => $this->faker->realText(),
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
    }

    protected function createProductAstrologer()
    {
        $astrologers = User::whereRole('astrologer')->get(['id']);
        $productsIds = Product::all()->pluck(['id'])->toArray();

        foreach ($astrologers as $astrologer) {
            $randomProductsIds = $this->faker->randomElements($productsIds, 2);
            foreach ($randomProductsIds as $productsId) {
                DB::table('product_user')->insert([
                    'user_id' => $astrologer->id,
                    'product_id' => $productsId,
                    'price' => $this->faker->randomFloat(2, 5, 100),
                    'currency' => 'USD',
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }
    }
}
