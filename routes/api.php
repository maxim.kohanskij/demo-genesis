<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::apiResource('users', UserController::class);
Route::apiResource('orders', OrderController::class);
Route::get('/users/role/{role}', [UserController::class, 'indexByRole']);

Route::fallback(function () {
    return response()->json(['message' => 'Page Not Found.'], 404);
});
